
<!-- README.md is generated from README.Rmd. Please edit that file -->

# pprepr <img src="man/figures/hex-pprepr.png" align="right" height="139" />

<!-- badges: start -->

[![GitLab CI Build
Status](https://gitlab.com/dickoa/pprepr/badges/master/pipeline.svg)](https://gitlab.com/dickoa/pprepr/pipelines)
[![Codecov Code
Coverage](https://codecov.io/gl/dickoa/pprepr/branch/master/graph/badge.svg)](https://codecov.io/gl/dickoa/pprepr)
[![CRAN
status](http://www.r-pkg.org/badges/version/pprepr)](http://www.r-pkg.org/pkg/pprepr)
<!-- badges: end -->

R package to validate and automatically repair planar partitions using
the [`pprepair`](https://github.com/tudelft3d/pprepair) Cpp tool.

## Installation

The `pprepair` Cpp library need these two libraries to compile:

  - [`CGAL`](https://www.cgal.org/)
  - [`GDAL`](https://gdal.org/)

The R package `pprepr` solves the CGAL dependencies by using the
[`cgal4h`](https://gitlab.com/dickoa/cgal4h) that expose CGAL 4 headers.
We use [`rwinlib`](https://github.com/rwinlib) to provide `GDAL` on
Windows in order to build this package from source. You will need the
latest version of
[`rtools`](https://cran.r-project.org/bin/windows/Rtools/) in order to
build the source code on Windows.

`pprepair` can also use these optional libraries:

  - [`GMP`](https://gmplib.org/)
  - [`MPFR`](https://www.mpfr.org/)

They are disabled by default on Windows but required if you want to
build the package in a Linux/OS X environment unless you change the
configure.ac file. After installing all these libraries, you can now
install the development version of the `pprepr` R package from
[Gitlab](https://gitlab.com/dickoa/pprepair) using the `remotes` R
package with:

``` r
# install.packages("remotes")
remotes::install_gitlab("dickoa/pprepr")
```
