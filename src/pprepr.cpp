// [[Rcpp::depends(cgal4h)]]

#include "PlanarPartition.h"
#include <Rcpp.h>

RCPP_MODULE(pp) {
  Rcpp::class_<PlanarPartition>("PlanarPartition")
    .constructor()
    .method("addOGRFeaturesFromSF", &PlanarPartition::addOGRFeaturesFromSF)
    .method("buildPP", &PlanarPartition::buildPP)
    .method("isValid", &PlanarPartition::isValid)
    .method("repair", &PlanarPartition::repair)
    .method("reconstructPolygons", &PlanarPartition::reconstructPolygons)
    .method("exportPolygonsFeaturesToSF", &PlanarPartition::exportPolygonsFeaturesToSF)
    .method("exportProblemRegionsToSF", &PlanarPartition::exportProblemRegionsToSF)
    .method("noPolygons", &PlanarPartition::noPolygons);
}
