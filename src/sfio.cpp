#include "sfio.h"

// Returns errors to R
// Note only case 4 actually returns immediately
// Lower error codes are recoverable
static void __err_handler(CPLErr eErrClass, int err_no, const char *msg)
{
  switch ( eErrClass )
    {
    case 0:
      break; // #nocov
    case 1:
    case 2:
      Rf_warning("GDAL Message %d: %s\n", err_no, msg); // #nocov
      break; // #nocov
    case 3:
      Rf_warning("GDAL Error %d: %s\n", err_no, msg);
      break;
    case 4:
      Rf_warning("GDAL Error %d: %s\n", err_no, msg); // #nocov
      Rcpp::stop("Unrecoverable GDAL error\n"); // #nocov
      break;
    default:
      Rf_warning("Received invalid error class %d (errno %d: %s)\n", eErrClass, err_no, msg); // #nocov
      break; // #nocov
    }
  return;
}

// #nocov start
static void __err_silent(CPLErr eErrClass, int err_no, const char *msg)
{
  return;
}
// #nocov end

void set_error_handler(void)
{
  CPLSetErrorHandler((CPLErrorHandler)__err_handler);
}

void unset_error_handler(void)
{
  CPLSetErrorHandler((CPLErrorHandler)__err_silent);
}


// borrowed from sf source code, (c) Edzer Pebesma
void handle_error(OGRErr err) {
  if (err != OGRERR_NONE) {
    switch (err) {
    case OGRERR_NOT_ENOUGH_DATA:
      Rcpp::Rcout << "OGR: Not enough data " << std::endl;
      break;
    case OGRERR_UNSUPPORTED_GEOMETRY_TYPE:
      Rcpp::Rcout << "OGR: Unsupported geometry type" << std::endl;
      break;
    case OGRERR_CORRUPT_DATA:
      Rcpp::Rcout << "OGR: Corrupt data" << std::endl;
      break;
    case OGRERR_FAILURE:
      Rcpp::Rcout << "OGR: index invalid?" << std::endl;
      break;
    default:
      Rcpp::Rcout << "Error code: " << err << std::endl;
    }
    Rcpp::stop("OGR error");
  }
}

// modified version of sf source code, (c) Edzer Pebesma
Rcpp::List create_na_crs() {
  Rcpp::List crs(2);
  crs(0) = Rcpp::CharacterVector::create(NA_STRING);
  crs(1) = Rcpp::CharacterVector::create(NA_STRING);
  Rcpp::CharacterVector nms(2);
  nms(0) = "input";
  nms(1) = "wkt";
  crs.attr("names") = nms;
  crs.attr("class") = "crs";
  return crs;
}

// borrowed from sf source code, (c) Edzer Pebesma
std::vector<OGRGeometry *> ogr_geometry_from_sfc(Rcpp::List sfc) {

  Rcpp::List wkblst = sf::CPL_write_wkb(sfc, false);
  std::vector<OGRGeometry *> g(sfc.length());

  for (int i = 0; i < wkblst.length(); i++) {
    Rcpp::RawVector r = wkblst[i];
    OGRErr err = OGRGeometryFactory::createFromWkb(&(r[0]), NULL, &(g[i]),
						   r.length(), wkbVariantIso);
    if (err != OGRERR_NONE) {
      if (g[i] != NULL)
	OGRGeometryFactory::destroyGeometry(g[i]);
      handle_error(err);
    }
  }
  return g;
}

// modified version of sf source code, (c) Edzer Pebesma
Rcpp::List sfc_from_ogr_geometry(std::vector<OGRGeometry *> g, bool destroy) {
  OGRwkbGeometryType type = wkbGeometryCollection;
  Rcpp::List lst(g.size());
  for (size_t i = 0; i < g.size(); i++) {
    if (g[i] == NULL)
      g[i] = OGRGeometryFactory::createGeometry(type);
    else
      type = g[i]->getGeometryType();
    Rcpp::RawVector raw(g[i]->WkbSize());
    handle_error(g[i]->exportToWkb(wkbNDR, &(raw[0]), wkbVariantIso));
    lst[i] = raw;
    if (destroy)
      OGRGeometryFactory::destroyGeometry(g[i]);
  }
  Rcpp::List ret = sf::CPL_read_wkb(lst, false, false);
  ret.attr("class") = "sfc";
  return ret;
}

unsigned int make_type(const char *cls, const char *dim, bool EWKB, int *tp,
		       int srid) {
  int type = 0;
  if (strstr(cls, "sfc_") == cls)
    cls += 4;
  if (strcmp(cls, "POINT") == 0)
    type = SF_Point;
  else if (strcmp(cls, "LINESTRING") == 0)
    type = SF_LineString;
  else if (strcmp(cls, "POLYGON") == 0)
    type = SF_Polygon;
  else if (strcmp(cls, "MULTIPOINT") == 0)
    type = SF_MultiPoint;
  else if (strcmp(cls, "MULTILINESTRING") == 0)
    type = SF_MultiLineString;
  else if (strcmp(cls, "MULTIPOLYGON") == 0)
    type = SF_MultiPolygon;
  else if (strcmp(cls, "GEOMETRYCOLLECTION") == 0)
    type = SF_GeometryCollection;
  else if (strcmp(cls, "CIRCULARSTRING") == 0)
    type = SF_CircularString;
  else if (strcmp(cls, "COMPOUNDCURVE") == 0)
    type = SF_CompoundCurve;
  else if (strcmp(cls, "CURVEPOLYGON") == 0)
    type = SF_CurvePolygon;
  else if (strcmp(cls, "MULTICURVE") == 0)
    type = SF_MultiCurve;
  else if (strcmp(cls, "MULTISURFACE") == 0)
    type = SF_MultiSurface;
  else if (strcmp(cls, "CURVE") == 0)
    type = SF_Curve; // #nocov
  else if (strcmp(cls, "SURFACE") == 0)
    type = SF_Surface; // #nocov
  else if (strcmp(cls, "POLYHEDRALSURFACE") == 0)
    type = SF_PolyhedralSurface;
  else if (strcmp(cls, "TIN") == 0)
    type = SF_TIN;
  else if (strcmp(cls, "TRIANGLE") == 0)
    type = SF_Triangle;
  else
    type = SF_Unknown; // a mix: GEOMETRY
  if (tp != NULL)
    *tp = type;
  if (EWKB) {
    if (strcmp(dim, "XYZ") == 0)
      type = type | EWKB_Z_BIT;
    else if (strcmp(dim, "XYM") == 0)
      type = type | EWKB_M_BIT;
    else if (strcmp(dim, "XYZM") == 0)
      type = type | EWKB_M_BIT | EWKB_Z_BIT;
    if (srid != 0)
      type = type | EWKB_SRID_BIT;
  } else {
    if (strcmp(dim, "XYZ") == 0)
      type += 1000;
    else if (strcmp(dim, "XYM") == 0)
      type += 2000;
    else if (strcmp(dim, "XYZM") == 0)
      type += 3000;
  }
  return type;
}


std::vector<char *> create_options(Rcpp::CharacterVector lco, bool quiet) {
  if (lco.size() == 0)
    quiet = true; // nothing to report
  if (! quiet)
    Rcpp::Rcout <<  "options:        "; // #nocov
  std::vector<char *> ret(lco.size() + 1);
  for (int i = 0; i < lco.size(); i++) {
    ret[i] = (char *) (lco[i]);
    if (! quiet)
      Rcpp::Rcout << ret[i] << " "; // #nocov
  }
  ret[lco.size()] = NULL;
  if (! quiet)
    Rcpp::Rcout << std::endl;         // #nocov
  return ret;
}


std::vector<OGRFieldType> SetupFields(OGRLayer *poLayer, Rcpp::List obj, bool update_layer) {
  std::vector<OGRFieldType> ret(obj.size());
  Rcpp::CharacterVector cls = obj.attr("colclasses");
  Rcpp::CharacterVector nm  = obj.attr("names");
  for (int i = 0; i < obj.size(); i++) {
    if (strcmp(cls[i], "character") == 0)
      ret[i] = OFTString;
    else if (strcmp(cls[i], "integer") == 0 || strcmp(cls[i], "logical") == 0)
      ret[i] = OFTInteger;
    else if (strcmp(cls[i], "numeric") == 0)
      ret[i] = OFTReal;
    else if (strcmp(cls[i], "Date") == 0)
      ret[i] = OFTDate;
    else if (strcmp(cls[i], "POSIXct") == 0)
      ret[i] = OFTDateTime;
    else { // #nocov start
      Rcpp::Rcout << "Field " << nm[i] << " of type " << cls[i] << " not supported." << std::endl;
      Rcpp::stop("Layer creation failed.\n");
    }      // #nocov end
    OGRFieldDefn oField(nm[i], ret[i]);
    if (strcmp(cls[i], "logical") == 0)
      oField.SetSubType(OFSTBoolean);
    if (!update_layer && poLayer->CreateField(&oField) != OGRERR_NONE) { // #nocov start
      Rcpp::Rcout << "Creating field " << nm[i] << " failed." << std::endl;
      Rcpp::stop("Layer creation failed.\n");
    } // #nocov end
  }
  return ret;
}

// this is like an unlist -> dbl, but only does the first 6; if we'd do unlist on the POSIXlt
// object, we'd get a character vector...
Rcpp::NumericVector get_dbl6(Rcpp::List in) {
  Rcpp::NumericVector ret(6);
  for (int i = 0; i < 6; i++) {
    Rcpp::NumericVector x = in(i);
    ret(i) = x(0);
  }
  return ret;
}

void SetNull(OGRFeature *poFeature, size_t field) {
#if (GDAL_VERSION_MINOR >= 2 || GDAL_VERSION_MAJOR > 2)
  poFeature->SetFieldNull(field);
#else
  poFeature->UnsetField(field);
#endif
}

void SetFields(OGRFeature *poFeature, std::vector<OGRFieldType> tp, Rcpp::List obj, size_t i, bool shape) {
  Rcpp::CharacterVector nm  = obj.attr("names");
  for (size_t j = 0; j < tp.size(); j++) {
    if (i == 0 && poFeature->GetFieldIndex(nm[j]) == -1) {
      Rcpp::Rcout << "Unknown field name `" << nm[j] <<
	"': updating a layer with improper field name(s)?" << std::endl;
      Rcpp::stop("Write error\n");
    }
    if (j == (size_t) poFeature->GetFieldCount())
      Rcpp::stop("Impossible: field count reached\n"); // #nocov
    switch (tp[j]) {
    case OFTString: {
      Rcpp::CharacterVector cv;
      cv = obj[j];
      if (! Rcpp::CharacterVector::is_na(cv[i])) {
	if (shape)
	  poFeature->SetField(j, (const char *) cv[i]);
	else
	  poFeature->SetField(nm[j], (const char *) cv[i]);
      } else
	SetNull(poFeature, j);
    } break;
    case OFTInteger: {
      Rcpp::IntegerVector iv;
      iv = obj[j];
      if (! Rcpp::IntegerVector::is_na(iv[i])) {
	if (shape)
	  poFeature->SetField(j, (int) iv[i]);
	else
	  poFeature->SetField(nm[j], (int) iv[i]);
      } else
	SetNull(poFeature, j); // #nocov
    } break;
    case OFTReal: {
      Rcpp::NumericVector nv;
      nv = obj[j];
      if (! Rcpp::NumericVector::is_na(nv[i])) {
	if (shape)
	  poFeature->SetField(j, (double) nv[i]);
	else
	  poFeature->SetField(nm[j], (double) nv[i]);
      } else
	SetNull(poFeature, j);
    } break;
    case OFTDate: {
      Rcpp::NumericVector nv;
      nv = obj[j];
      if (Rcpp::NumericVector::is_na(nv[i])) {
	SetNull(poFeature, j);
	break;
      }
      Rcpp::NumericVector nv0(1);
      nv0[0] = nv[i];
      nv0.attr("class") = "Date";
      Rcpp::Function as_POSIXlt_Date("as.POSIXlt.Date");
      Rcpp::Function unlist("unlist");
      Rcpp::NumericVector ret = unlist(as_POSIXlt_Date(nv0)); // use R
      if (shape)
	poFeature->SetField(j, 1900 + (int) ret[5], (int) ret[4] + 1, (int) ret[3]);
      else
	poFeature->SetField(nm[j], 1900 + (int) ret[5], (int) ret[4] + 1, (int) ret[3]);
    } break;
    case OFTDateTime: {
      Rcpp::NumericVector nv;
      nv = obj[j];
      if (Rcpp::NumericVector::is_na(nv[i])) {
	SetNull(poFeature, j);
	break;
      }
      Rcpp::NumericVector nv0(1);
      nv0[0] = nv[i];
      nv0.attr("tzone") = "UTC";
      Rcpp::Function as_POSIXlt_POSIXct("as.POSIXlt.POSIXct");
      Rcpp::NumericVector rd = get_dbl6(as_POSIXlt_POSIXct(nv0)); // use R
      if (shape)
	poFeature->SetField(j, 1900 + (int) rd[5], (int) rd[4] + 1, // #nocov start
			    (int) rd[3], (int) rd[2], (int) rd[1],
			    (float) rd[0], 100); // nTZFlag: 0=unkown, 1=local, 100=GMT; #nocov end
      else
	poFeature->SetField(nm[j], 1900 + (int) rd[5], (int) rd[4] + 1,
			    (int) rd[3], (int) rd[2], (int) rd[1],
			    (float) rd[0], 100); // nTZFlag 0: unkown; 1: local; 100: GMT
    } break;
    default:
      // we should never get here! // #nocov start
      Rcpp::Rcout << "field with unsupported type ignored" << std::endl;
      Rcpp::stop("Layer creation failed.\n");
      break; // #nocov end
    }
  }
}

std::vector<OGRFeature*> ogr_features_from_sf(Rcpp::List obj, Rcpp::List geom) {

  const char *dsn = "/vsimem/pprepair_dummy_raw.shp";
  const char *driver = "ESRI Shapefile";
  const char *layer = "dummy";
  const char *dim = "XY";
  bool quiet = false;

  /* GDALAllRegister(); -- has been done during .onLoad() */
  // get driver:
  GDALDriver *poDriver = GetGDALDriverManager()->GetDriverByName(driver);
  if (poDriver == NULL) {
    Rcpp::Rcout << "driver `" << driver << "' not available." << std::endl;
    Rcpp::stop("Driver not available.\n");
  }

  std::vector <char *> drivers = create_options(driver, true);

  // data set:
  GDALDataset *poDS;

  if ((poDS = poDriver->Create(dsn, 0, 0, 0, GDT_Unknown, NULL)) == NULL) {
    Rcpp::stop("Creation of virtual file failed.\n");
  }

  // can & do transaction?
  bool can_do_transaction = (poDS->TestCapability(ODsCTransactions) == TRUE); // can?
  bool transaction = false;
  if (can_do_transaction) { // try to start transaction:
    unset_error_handler();
    transaction = (poDS->StartTransaction() == OGRERR_NONE); // do?
    set_error_handler();
    if (! transaction) { // failed: #nocov start
      GDALClose(poDS);
      Rcpp::stop("Write error.\n");
    } // #nocov end
  }

  Rcpp::CharacterVector clsv = geom.attr("class");
  OGRwkbGeometryType wkbType = (OGRwkbGeometryType) make_type(clsv[0], dim, false, NULL, 0);
  // read geometries:
  std::vector<OGRGeometry *> geomv = ogr_geometry_from_sfc(geom);
  std::vector<OGRFeature*> out(geomv.size());

  // create layer:
  OGRLayer *poLayer;
  poLayer = poDS->CreateLayer(layer, NULL, wkbType, NULL);
  if (poLayer == NULL)  {
    Rcpp::Rcout << "Creating or updating layer " << layer << " failed." << std::endl;
    GDALClose(poDS);
    Rcpp::stop("Write error.\n");
  }

  // write feature attribute fields & geometries:
  std::vector<OGRFieldType> fieldTypes = SetupFields(poLayer, obj, false);
  for (size_t i = 0; i < geomv.size(); i++) { // create all features & add to layer:
    OGRFeature *poFeature = OGRFeature::CreateFeature(poLayer->GetLayerDefn());
    SetFields(poFeature, fieldTypes, obj, i, driver == "ESRI Shapefile");
    poFeature->SetGeometryDirectly(geomv[i]);
    if (poLayer->CreateFeature(poFeature) != OGRERR_NONE) {
      Rcpp::Rcout << "Failed to create feature " << i << " in " << layer << std::endl;
      // delete layer when  failing to  create feature
      OGRErr err = poDS->DeleteLayer(0);
      GDALClose(poDS);
      if (err != OGRERR_NONE) { // #nocov start
	if (err == OGRERR_UNSUPPORTED_OPERATION)
	  Rcpp::Rcout << "Deleting layer not supported by driver `" << driver << "'" << std::endl;
	else if (! transaction)
	  Rcpp::Rcout << "Deleting layer `" << layer << "' failed" << std::endl;
      } // #nocov end
      OGRFeature::DestroyFeature(poFeature);
      if (!transaction)
	Rcpp::stop("Feature creation failed.\n");
    }
    out[i] = poFeature;
  }
  if (transaction && poDS->CommitTransaction() != OGRERR_NONE) { // #nocov start
    poDS->RollbackTransaction();
    GDALClose(poDS);
    Rcpp::stop("CommitTransaction() failed.\n");
  } // #nocov end
  poDriver->Delete(dsn);
  GDALClose(poDS);
  return out; // all O.K.
}

Rcpp::List allocate_out_list(OGRFeatureDefn *poFDefn, int n_features, bool int64_as_string,
			     Rcpp::CharacterVector fid_column) {

  if (fid_column.size() > 1)
    Rcpp::stop("FID column name should be a length 1 character vector"); // #nocov

  int n = poFDefn->GetFieldCount() + poFDefn->GetGeomFieldCount() + fid_column.size();
  Rcpp::List out(n);
  Rcpp::CharacterVector names(n);
  for (int i = 0; i < poFDefn->GetFieldCount(); i++) {
    OGRFieldDefn *poFieldDefn = poFDefn->GetFieldDefn(i);
    switch (poFieldDefn->GetType()) {
    case OFTInteger: {
      if (poFieldDefn->GetSubType() == OFSTBoolean)
	out[i] = Rcpp::LogicalVector(n_features);
      else
	out[i] = Rcpp::IntegerVector(n_features);
    }
      break;
    case OFTDate: {
      Rcpp::NumericVector ret(n_features);
      ret.attr("class") = "Date";
      out[i] = ret;
    } break;
    case OFTDateTime: {
      Rcpp::NumericVector ret(n_features);
      Rcpp::CharacterVector cls(2);
      cls(0) = "POSIXct";
      cls(1) = "POSIXt";
      ret.attr("class") = cls;
      out[i] = ret;
    } break;
    case OFTInteger64: // fall through: converts Int64 -> double
      if (int64_as_string)
	out[i] = Rcpp::CharacterVector(n_features);
      else
	out[i] = Rcpp::NumericVector(n_features);
      break;
    case OFTReal:
      out[i] = Rcpp::NumericVector(n_features);
      break;
    case OFTStringList:
    case OFTRealList:
    case OFTIntegerList:
    case OFTInteger64List:
      out[i] = Rcpp::List(n_features);
      break;
    case OFTString:
    default:
      out[i] = Rcpp::CharacterVector(n_features);
      break;
    }
    names[i] = poFieldDefn->GetNameRef();
  }

  if (fid_column.size())
    names[ poFDefn->GetFieldCount() ] = fid_column[0];

  for (int i = 0; i < poFDefn->GetGeomFieldCount(); i++) {
    // get the geometry fields:
    OGRGeomFieldDefn *poGFDefn = poFDefn->GetGeomFieldDefn(i);
    if (poGFDefn == NULL)
      Rcpp::stop("GeomFieldDefn error"); // #nocov
    std::string geom = "geometry";
    const char *geom_name = poGFDefn->GetNameRef();
    if (*geom_name == '\0') {
      if (i > 0)
	names[i + poFDefn->GetFieldCount() + fid_column.size()] = geom + std::to_string(i); // c++11; #nocov
      else
	names[i + poFDefn->GetFieldCount() + fid_column.size()] = geom;
    } else
      names[i + poFDefn->GetFieldCount() + fid_column.size()] = geom_name;
    out[i + poFDefn->GetFieldCount() + fid_column.size()] = Rcpp::List(n_features); // ?
  }

  out.attr("names") = names;
  return out;
}

int to_multi_what(std::vector<OGRGeometry *> gv) {
  bool points = false, multipoints = false,
    lines = false, multilines = false,
    polygons = false, multipolygons = false;

  for (unsigned int i = 0; i < gv.size(); i++) {
    // drop Z and M:
    if (gv[i] == NULL)
      break;
    OGRwkbGeometryType gt = OGR_GT_SetModifier(gv[i]->getGeometryType(), 0, 0);
    switch(gt) {
    case wkbPoint: points = true; break;
    case wkbMultiPoint: multipoints = true; break;
    case wkbLineString: lines = true; break;
    case wkbMultiLineString: multilines = true; break;
    case wkbPolygon: polygons = true; break;
    case wkbMultiPolygon: multipolygons = true; break;
    default: return 0; // #nocov
    }
  }
  int sum = points + multipoints + lines + multilines + polygons + multipolygons;
  if (sum == 2) {
    if (points && multipoints)
      return wkbMultiPoint;
    if (lines && multilines)
      return wkbMultiLineString;
    if (!lines && !multilines)
      return wkbMultiPolygon;
  }
  // another mix or single type:
  return 0;
}

Rcpp::List sf_from_ogr_features(std::vector<OGRFeature*> inputFeatures, bool quiet, bool int64_as_string,
				Rcpp::NumericVector toTypeUser, Rcpp::CharacterVector fid_column, bool promote_to_multi) {

  size_t n = inputFeatures.size();
  std::vector<OGRFeature *> poFeatureV(n); // full archive
  Rcpp::CharacterVector fids(n);

  OGRFeatureDefn *poFDefn = inputFeatures[0]->GetDefnRef();

  std::vector<OGRGeometry *> poGeometryV(n * poFDefn->GetGeomFieldCount());
  // cycles column wise: 2nd el is 1st geometry, 2nd feature

  Rcpp::List out = allocate_out_list(poFDefn, n, int64_as_string, fid_column);

  // read all features:
  size_t i = 0; // feature counter
  double dbl_max_int64 = pow(2.0, 53);
  bool warn_int64 = false, has_null_geometries = false;
  OGRFeature *poFeature;
  for (i = 0; i < n; i++) {
    poFeature = inputFeatures[i];
    // getFID:
    fids[i] = std::to_string(poFeature->GetFID());

    // feature attribute fields:
    for (int iField = 0; iField < poFDefn->GetFieldCount(); iField++ ) {
      OGRFieldDefn *poFieldDefn = poFDefn->GetFieldDefn( iField );
#if (GDAL_VERSION_MINOR >= 2 || GDAL_VERSION_MAJOR > 2)
      int not_NA = poFeature->IsFieldSetAndNotNull(iField);
#else
      int not_NA = poFeature->IsFieldSet(iField);
#endif
      switch(poFieldDefn->GetType()) {
      case OFTInteger: {
	if (poFieldDefn->GetSubType() == OFSTBoolean) {
	  Rcpp::LogicalVector lv;
	  lv = out[iField];
	  if (not_NA) {
	    if (poFeature->GetFieldAsInteger(iField))
	      lv[i] = true;
	    else
	      lv[i] = false;
	  } else
	    lv[i] = NA_LOGICAL;
	} else {
	  Rcpp::IntegerVector iv;
	  iv = out[iField];
	  if (not_NA)
	    iv[i] = poFeature->GetFieldAsInteger(iField);
	  else
	    iv[i] = NA_INTEGER;
	}
      }
	break;
      case OFTInteger64: {
	if (int64_as_string) {
	  Rcpp::CharacterVector cv;
	  cv = out[iField];
	  if (not_NA)
	    cv[i] = poFeature->GetFieldAsString(iField);
	  else
	    cv[i] = NA_STRING;
	} else {
	  Rcpp::NumericVector nv;
	  nv = out[iField];
	  if (not_NA)
	    nv[i] = (double) poFeature->GetFieldAsInteger64(iField);
	  else
	    nv[i] = NA_REAL;
	  // OR: poFeature->GetFieldAsString(iField);
	  if (nv[i] > dbl_max_int64)
	    warn_int64 = true;
	}
      }
	break;
      case OFTDateTime:
      case OFTDate: {
	int Year, Month, Day, Hour, Minute, TZFlag;
	float Second;
	poFeature->GetFieldAsDateTime(iField, &Year, &Month, &Day, &Hour, &Minute,
				      &Second, &TZFlag);
	//  POSIXlt: sec   min  hour  mday   mon  year  wday  yday isdst ...
	Rcpp::List dtlst =
	  Rcpp::List::create((double) Second, (double) Minute,
			     (double) Hour, (double) Day, (double) Month - 1, (double) Year - 1900,
			     0.0, 0.0, 0.0);
	dtlst.attr("class") = "POSIXlt";
	if (TZFlag == 100)
	  dtlst.attr("tzone") = "UTC";
	Rcpp::NumericVector nv;
	nv = out[iField];
	if (! not_NA) {
	  nv[i] = NA_REAL;
	  break;
	}
	if (poFieldDefn->GetType() == OFTDateTime) {
	  Rcpp::Function as_POSIXct_POSIXlt("as.POSIXct.POSIXlt");
	  Rcpp::NumericVector ret = as_POSIXct_POSIXlt(dtlst); // R help me!
	  nv[i] = ret[0];
	} else {
	  Rcpp::Function as_Date_POSIXlt("as.Date.POSIXlt");
	  Rcpp::NumericVector ret = as_Date_POSIXlt(dtlst); // R help me!
	  nv[i] = ret[0];
	}
	break;
      }
	break;
      case OFTReal: {
	Rcpp::NumericVector nv;
	nv = out[iField];
	if (not_NA)
	  nv[i] = (double) poFeature->GetFieldAsDouble(iField);
	else
	  nv[i] = NA_REAL;
      }
	break;
      case OFTStringList: {
	Rcpp::List lv;
	lv = out[iField];
	char **sl = poFeature->GetFieldAsStringList(iField);
	Rcpp::CharacterVector cv(CSLCount(sl));
	for (int j = 0; j < cv.size(); j++)
	  cv[j] = sl[j];
	lv[i] = cv;
      }
	break;
      case OFTRealList: {
	// for all *List types, NA is handled by zero-length lists
	Rcpp::List lv; // #nocov start
	lv = out[iField];
	int n;
	const double *dl = poFeature->GetFieldAsDoubleList(iField, &n);
	Rcpp::NumericVector nv(n);
	for (int j = 0; j < nv.size(); j++)
	  nv[j] = dl[j];
	lv[i] = nv;
      }
	break; // #nocov end
      case OFTIntegerList: {
	Rcpp::List lv;
	lv = out[iField];
	int n;
	const int *il = poFeature->GetFieldAsIntegerList(iField, &n);
	Rcpp::IntegerVector iv(n);
	for (int j = 0; j < iv.size(); j++)
	  iv[j] = il[j];
	lv[i] = iv;
      }
	break;
      case OFTInteger64List: {
	Rcpp::List lv;
	lv = out[iField];
	int n;
	const GIntBig *int64list = poFeature->GetFieldAsInteger64List(iField, &n);
	if (int64_as_string) {
	  Rcpp::CharacterVector cv(n);
	  for (int j = 0; j < cv.size(); j++) {
	    std::stringstream stream;
	    stream << int64list[j];
	    cv[j] = stream.str();
	  }
	  lv[i] = cv;
	} else {
	  Rcpp::NumericVector nv(n);
	  for (int j = 0; j < nv.size(); j++) {
	    nv[j] = (double) int64list[j];
	    if (nv[j] > dbl_max_int64)
	      warn_int64 = true; // #nocov
	  }
	  lv[i] = nv;
	}
      }
	break;
      default: // break through: anything else to be converted to string?
      case OFTString: {
	Rcpp::CharacterVector cv;
	cv = out[iField];
	if (not_NA)
	  cv[i] = poFeature->GetFieldAsString(iField);
	else
	  cv[i] = NA_STRING;
      }
	break;
      }
    }

    // feature geometry:
    for (int iGeom = 0; iGeom < poFDefn->GetGeomFieldCount(); iGeom++ ) {
      poGeometryV[i + n * iGeom] = poFeature->GetGeomFieldRef(iGeom);
      if (poGeometryV[i + n * iGeom] == NULL)
	has_null_geometries = true;
    }

    poFeatureV[i] = poFeature;
  } // all read...

  // add feature IDs if needed:
  if (fid_column.size())
    out[ poFDefn->GetFieldCount() ] = fids;

  std::vector<OGRGeometry *> to_be_freed;
  for (int iGeom = 0; iGeom < poFDefn->GetGeomFieldCount(); iGeom++ ) {

    std::vector<OGRGeometry *> poGeom(n);
    for (i = 0; i < n; i++)
      poGeom[i] = poGeometryV[i + n * iGeom];

    int toType = 0, toTypeU = 0;
    if (toTypeUser.size() == poFDefn->GetGeomFieldCount())
      toTypeU = toTypeUser[iGeom];
    else
      toTypeU = toTypeUser[0];
    if (promote_to_multi && toTypeU == 0)
      toType = to_multi_what(poGeom);
    else
      toType = toTypeU;

    if (toType != 0) { // coerce to toType:
      // OGRGeomFieldDefn *poGFDefn = poFDefn->GetGeomFieldDefn(i);
      for (i = 0; i < poFeatureV.size(); i++) {
	OGRGeometry *geom = poFeatureV[i]->StealGeometry(iGeom); // transfer ownership
	if (geom == NULL)
	  geom = OGRGeometryFactory::createGeometry((OGRwkbGeometryType) toType); // #nocov
	else if ((geom =
		  OGRGeometryFactory::forceTo(geom, (OGRwkbGeometryType) toType, NULL))
		 == NULL)
	  Rcpp::stop("OGRGeometryFactory::forceTo returned NULL"); // #nocov
	handle_error(poFeatureV[i]->SetGeomFieldDirectly(iGeom, geom));
	poGeom[i] = poFeatureV[i]->GetGeomFieldRef(iGeom);
	if (poGeom[i] == NULL)
	  Rcpp::stop("GetGeomFieldRef returned NULL"); // #nocov
      }
    } else if (has_null_geometries) {
      if (! quiet)
	Rcpp::Rcout << "replacing null geometries with empty geometries" << std::endl; // #nocov

      // replace null's with empty:
      OGRwkbGeometryType gt = wkbGeometryCollection;
      for (i = 0; i < poGeom.size(); i++) {
	if (poGeom[i] != NULL) {
	  gt = poGeom[i]->getGeometryType(); // first non-NULL
	  break;
	}
      }
      for (i = 0; i < poGeom.size(); i++) {
	if (poGeom[i] == NULL) {
	  poGeom[i] = OGRGeometryFactory::createGeometry(gt);
	  if (poGeom[i] == NULL)
	    Rcpp::stop("createGeometry returned NULL"); // #nocov
	  else
	    to_be_freed.push_back(poGeom[i]);
	}
      }
    }
    if (! quiet && toTypeU != 0 && n > 0)
      Rcpp::Rcout << "converted into: " << poGeom[0]->getGeometryName() << std::endl; // #nocov
    // convert to R:
    Rcpp::List sfc = sfc_from_ogr_geometry(poGeom, false); // don't destroy
    OGRGeomFieldDefn *fdfn = poFDefn->GetGeomFieldDefn(iGeom);
    sfc.attr("crs") = create_na_crs(); // overwrite: see #449 for the reason why
    // sfc.attr("crs") = create_crs(fdfn->GetSpatialRef()); // overwrite: see #449 for the reason why
    out[iGeom + poFDefn->GetFieldCount() + fid_column.size()] = sfc;
  }

  if (warn_int64)
    Rcpp::Rcout << "Integer64 values larger than " << dbl_max_int64 <<
      " lost significance after conversion to double;" << std::endl <<
      "use argument int64_as_string = TRUE to import them lossless, as character" << std::endl;

  // clean up:
  for (i = 0; i < n; i++)
    OGRFeature::DestroyFeature(poFeatureV[i]);
  for (i = 0; i < to_be_freed.size(); i++)
    OGRGeometryFactory::destroyGeometry(to_be_freed[i]);

  return out;
}

// [[Rcpp::export]]
bool CPL_gdal_destroy(Rcpp::CharacterVector dsn) {
  GDALDatasetH ds = GDALOpenEx((const char *) dsn[0], GDAL_OF_VECTOR, NULL, NULL, NULL);
  if (ds == NULL)
    Rcpp::stop("Source not available!"); // #nocov
  GetGDALDriverManager()->GetDriverByName("ESRI Shapefile")->Delete(dsn[0]);
  GDALClose(ds);
  return true;
}

int rrand(const int max) {
  double u = R::runif(0, max);
  return std::floor(u);
}
