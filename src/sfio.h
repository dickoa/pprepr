#ifndef SFIO
#define SFIO

#include <ogrsf_frmts.h>
#include <Rcpp.h>

// [[Rcpp::depends(sf)]]
#include <sf.h>

#define EWKB_Z_BIT    0x80000000
#define EWKB_M_BIT    0x40000000
#define EWKB_SRID_BIT 0x20000000

/*      NULL/EMPTY             0 */
#define SF_Unknown             0 /* sfc_GEOMETRY */
#define SF_Point               1
#define SF_LineString          2
#define SF_Polygon             3
#define SF_MultiPoint          4
#define SF_MultiLineString     5
#define SF_MultiPolygon        6
#define SF_GeometryCollection  7
#define SF_CircularString      8
#define SF_CompoundCurve       9
#define SF_CurvePolygon       10
#define SF_MultiCurve         11
#define SF_MultiSurface       12
#define SF_Curve              13
#define SF_Surface            14
#define SF_PolyhedralSurface  15
#define SF_TIN                16
#define SF_Triangle           17

int rrand(const int max);
void __err_handler();
void set_error_handler(void);
void unset_error_handler(void);
void handle_error(OGRErr err);
std::vector<char *> create_options(Rcpp::CharacterVector lco, bool quiet);
Rcpp::List create_na_crs();
unsigned int make_type(const char *cls, const char *dim, bool EWKB = false, int *tp = NULL, int srid = 0);
std::vector<OGRFieldType> SetupFields(OGRLayer *poLayer, Rcpp::List obj, bool update_layer);
void SetFields(OGRFeature *poFeature, std::vector<OGRFieldType> tp, Rcpp::List obj, size_t i, bool shape);
Rcpp::List allocate_out_list(OGRFeatureDefn *poFDefn, int n_features, bool int64_as_string,
			     Rcpp::CharacterVector fid_column);
int to_multi_what(std::vector<OGRGeometry *> gv);
std::vector<OGRGeometry *> ogr_geometry_from_sfc(Rcpp::List sfc);
Rcpp::List sfc_from_ogr_geometry(std::vector<OGRGeometry *> g, bool destroy = false);
std::vector<OGRFeature*> ogr_features_from_sf(Rcpp::List obj, Rcpp::List geom);
Rcpp::List sf_from_ogr_features(std::vector<OGRFeature*> inputFeatures, bool quiet, bool int64_as_string,
				Rcpp::NumericVector toTypeUser, Rcpp::CharacterVector fid_column, bool promote_to_multi = true);

#endif